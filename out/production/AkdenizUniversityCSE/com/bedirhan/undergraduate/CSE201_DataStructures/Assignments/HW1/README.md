## Yönergeler
    ### Please submit a single word file. (NO ZIP,RAR) File name should be your student number.

- Write a short method in any language that counts the number of a,e,i,u characters in a given character string.


- Write a method that takes an array of float values and determines if all the numbers are different from each other (that is, they are distinct) up to 4 digits after point.


- Write a method that takes an array containing the set of all integers in the range 1 to 52 and shuffles it into random order. Your method should output each possible order with equal probability.