package com.bedirhan.undergraduate.CSE201_DataStructures.Practice;

import java.util.Arrays;

public class BinarySearchAlgo {
    public static void main(String[] args) {
        int[] arr =  {1,5,2,8,3,100,34,12};
        Arrays.sort(arr);
        reverseArrayRec(arr,0, arr.length-1);
        for (int a: arr) {
            System.out.println(a);
        }
    }

    public static boolean binarySearch(int[] arr,int target,int low,int high){
        int midPoint = (low+high)/2;
        if (target < arr[low] || target > arr[high]) {
            return false;
        }
        if (arr[midPoint] == target)
            return true;
        else if (arr[midPoint] > target) {
            return binarySearch(arr,target,low,(midPoint-1));
        }
        else if (arr[midPoint] < target){
            return binarySearch(arr,target,(midPoint+1),high);
        }
        else
            return false;
    }

    public static boolean binarySearch(int[] arr,int target){
        int low = 0;
        int high = arr.length-1;
        while (low<=high){
            int mid = (low+high)/2;
            if (target == mid)
                return true;
            else if (target < mid) {
                high = mid -1;
            }
            else {
                low = mid + 1;
            }
        }
        return false;
    }


    public static void reverseArrayRec(int[] data, int low, int high){
            if (low < high){
                int temp = data[high];
                data[high] = data[low];
                data[low] = temp;
                reverseArrayRec(data, low+1, high-1);
            }
    }
    public static void reverseArrayNonRec(int[] data, int low, int high)
    {
        while (low < high){
            int temp = data[high];
            data[high] = data[low];
            data[low] = temp;
            low+=1;
            high-=1;
        }
    }

    public static double power(int base,int power){
        if (power == 0)
            return 1;
        return base*power(base,power-1);
    }

}
