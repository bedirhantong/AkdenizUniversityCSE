package com.bedirhan.undergraduate.CSE201_DataStructures.Practice.DataStructures.Stack;

public class ArrayStack<E> implements Stack<E> {
    public static void main(String[] args) {
        ArrayStack<Integer> stack = new ArrayStack<>();
        stack.push(2);
        stack.push(3);
        stack.push(5);
        System.out.println(stack.pop());
        System.out.println(stack.counter);
        System.out.println(stack.top());
    }
    private E[] data;
    private static final int CAPACITY = 1000;
    int counter = -1;

    public ArrayStack(int capacity){
        data = (E[])new Object[capacity];
    }
    public ArrayStack(){
        this(CAPACITY);
    }

    @Override
    public boolean isEmpty() {
        return counter == -1;
    }

    @Override
    public E top() {
        if (isEmpty())
            return null;
        return data[counter];
    }

    @Override
    public E pop() {
        if (isEmpty())
            return null;
        E e = data[counter];
        data[counter] = null;
        counter--;
        return e;
    }

    @Override
    public int size() {
        return counter+1;
    }

    @Override
    public void push(E e) {
        if(size() == data.length){
            throw new IllegalStateException("Stack is full");
        }
        data[++counter] = e;
    }
}
