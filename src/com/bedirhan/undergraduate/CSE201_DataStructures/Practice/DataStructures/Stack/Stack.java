package com.bedirhan.undergraduate.CSE201_DataStructures.Practice.DataStructures.Stack;

public interface Stack<E> {
    boolean isEmpty();
    E top();
    E pop();
    int size();
    void push(E e);
}
