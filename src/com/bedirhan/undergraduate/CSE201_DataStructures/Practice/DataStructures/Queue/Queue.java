package com.bedirhan.undergraduate.CSE201_DataStructures.Practice.DataStructures.Queue;

public interface Queue<E> {
    boolean isEmpty();
    void enqueue(E e);
    E first();
    E dequeue();
    int size();
}
