package com.bedirhan.undergraduate.CSE201_DataStructures.Practice;

public class RecFac {
    public static void main(String[] args) {

    }

    static int fact(int n){
        if(n == 0)
            return 1;
        if (n < 0)
            throw new IllegalArgumentException();
        else
            return n*(fact(n-1));
    }

}
