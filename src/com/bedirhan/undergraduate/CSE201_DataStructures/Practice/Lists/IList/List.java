package com.bedirhan.undergraduate.CSE201_DataStructures.Practice.Lists.IList;

public interface List<E> {
    boolean isEmpty();
    int size();
    E get(int i);
    E set(int i, E e);
    void add(int i,E e);
    E remove(int i);
}
