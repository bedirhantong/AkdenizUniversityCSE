package com.bedirhan.undergraduate.CSE201_DataStructures.Practice.Lists.SinglyLİnkedList;

public class SinglyLinkedList<E> {
    Node<E> head = null;
    Node<E> tail = null;
    private int size = 0;

    public SinglyLinkedList(){};


    public E removeFirst(){
        if (isEmpty()){
            return null;
        }
        E answer = head.getElement();
        head = head.getNext();
        size--;
        if (size==0){
            tail = null;
        }
        return answer;
    }

    public void addFirst(E e){
        head = new Node<>(e,head);
        if (size == 0){
            tail = head;
        }
        size++;
    }

    public void addLast(E e){
        Node<E> newEst = new Node<>(e,null);
        if (isEmpty()){
            head=newEst;
        }else {
            tail.setNext(newEst);
        }
        tail = newEst;
        size++;
    }


    public void removeLast(){
        int index = getNodeIndex(tail)-1;
        Node<E> newEst = head;
        for (int i = 1; i < index; i++) {
            newEst.getNext();
        }
        newEst.setNext(null);
    }

    public int getNodeIndex(Node<E> node){
        Node<E> newNode = head;
        int index = 0;
        while (newNode != node){
            newNode = newNode.getNext();
            index++;
        }
        return index;
    }
    public Node<E> getNode(int index){
        Node<E> newNode = head;
        for (int i = 1; i < index; i++) {
            newNode = newNode.getNext();
        }
        return newNode;
    }

    public int getSize(){
        return this.size;
    }

    public boolean isEmpty(){
        return size==0;
    }

    public E first (){
        if (isEmpty())
            return null;
        return head.getElement();
    }

    public E last (){
        if (isEmpty())
            return null;
        return tail.getElement();
    }




}
class Node<E>{
    private E element;
    private Node<E> next;

    public Node(E element){
        this.element =  element;
    }
    public Node(E element,Node<E> next){
        this(element);
        this.next = next;
    }

    public E getElement(){
        return this.element;
    }
    public Node<E> getNext(){
        return this.next;
    }
    public void setNext(Node<E> nextNode){
        this.next = nextNode;
    }

}
