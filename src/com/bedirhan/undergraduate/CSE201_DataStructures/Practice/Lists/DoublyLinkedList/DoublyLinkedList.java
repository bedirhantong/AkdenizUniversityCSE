package com.bedirhan.undergraduate.CSE201_DataStructures.Practice.Lists.DoublyLinkedList;

public class DoublyLinkedList<E> {
    public static void main(String[] args) throws Exception {
        DoublyLinkedList<Integer> doublyLinkedList = new DoublyLinkedList<>();
        doublyLinkedList.addLast(0);
        doublyLinkedList.addLast(1);
        doublyLinkedList.insertAfter(0,4);
        doublyLinkedList.removeLast();

        doublyLinkedList.print();

    }
    Node<E> head;
    Node<E> tail;

    public boolean isEmpty(){
        return head == null;
    }




    public void printReverse(){
        printReverse(tail);
    }
    public void printReverse(Node<E> node){
        if (node == null)
            return;

        System.out.println(node.getValue());
        printReverse(node.getPrevious());
    }


    public void print(){
        print(head);
    }

    public void print(Node<E> node){
        if (node == null){
            return;
        }
        System.out.println(node.getValue());
        print(node.getNext());
    }

    public E first(){
        if (isEmpty()){
            return null;
        }
        return head.getValue();
    }

    public Node<E> getNode(int index){
        if (isEmpty() || index<0 ){
            return null;
        }

        if (head == null)
            return null;

        Node<E> temp = head;
        for (int i = 0; i < index; i++) {
            temp = temp.getNext();
        }
        return temp;
    }

    public void insertAfter(int index, E value){
        if (getNode(index)==null)
            throw new IllegalArgumentException();
        Node<E> mid = new Node<>(value);
        Node<E> next = getNode(index).getNext();
        getNode(index).setNext(mid);
        mid.setNext(next);
        next.setPrevious(mid);
    }

    public void addLast(E e){
        Node<E> temp = new Node<>(e);
        if (isEmpty()){
            head = temp;
        }else {
            tail.setNext(temp);
        }
        tail = temp;
    }

    public void removeLast() throws Exception {
        if (isEmpty()){
            throw new Exception("List is empty");
        }
        tail.getPrevious().setNext(null);

    }


}
class Node<E>{
    private Node<E> next;
    private Node<E> previous;
    private E value;

    public Node(E value){
        this.value = value;
    }

    public Node<E> getNext() {
        return next;
    }

    public void setNext(Node<E> next) {
        this.next = next;
    }

    public Node<E> getPrevious() {
        return previous;
    }

    public void setPrevious(Node<E> previous) {
        this.previous = previous;
    }

    public E getValue() {
        return value;
    }

}
