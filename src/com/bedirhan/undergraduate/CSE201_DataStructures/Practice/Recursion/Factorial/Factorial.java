package com.bedirhan.undergraduate.CSE201_DataStructures.Practice.Recursion.Factorial;

public class Factorial {
    public static int takeFactorialOf(int n){
        if (n < 0){
            throw new IllegalArgumentException("n cannot be negative");
        }
        else if (n == 0){
            return 1;
        }else {
            return n*takeFactorialOf(n-1);
        }
    }
}
