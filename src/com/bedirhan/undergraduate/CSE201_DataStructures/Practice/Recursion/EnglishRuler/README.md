# ENGLİSH RULER

![img.png](img.png)
-  For each inch, we place a tick with a numeric label. We denote the length
   of the tick designating a whole inch as the major tick length. Between the marks
   for whole inches, the ruler contains a series of minor ticks, placed at intervals of
   1/2 inch, 1/4 inch, and so on. As the size of the interval decreases by half, the tick
   length decreases by one