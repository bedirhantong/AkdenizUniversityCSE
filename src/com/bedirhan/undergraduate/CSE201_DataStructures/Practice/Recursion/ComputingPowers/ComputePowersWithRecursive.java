package com.bedirhan.undergraduate.CSE201_DataStructures.Practice.Recursion.ComputingPowers;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class ComputePowersWithRecursive {
    public static void main(String[] args) {
        System.out.println(computeWithLogN(2,3));
    }

    public static int computePowerWithRecursive(int base,int power){
        if (power == 0)
            return 1;
        else
            return base*computePowerWithRecursive(base,power-1);
    }

    public static int computeWithLogN(int base,int power){
        if (power == 0){
            return 1;
        }
        else {
            int partial = computeWithLogN(base,power/2);
            int result = partial*partial;
            if (power %2 ==1)
                result = result*base;
            return result;
        }
    }
}
