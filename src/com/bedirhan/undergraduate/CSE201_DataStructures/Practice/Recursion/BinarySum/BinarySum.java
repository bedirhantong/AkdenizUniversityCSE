package com.bedirhan.undergraduate.CSE201_DataStructures.Practice.Recursion.BinarySum;

public class BinarySum {
    public static void main(String[] args) {
        int[] arr = {1,2,3,4};
        System.out.println(binarySum(arr,0, arr.length-1));
    }

    public static int binarySum(int[] array,int lowIndex, int highIndex){
        if (lowIndex > highIndex)
            return 0;
        else if (lowIndex == highIndex) {
            return array[lowIndex];
        }else {
            int mid = (lowIndex + highIndex)/2;
            return binarySum(array, lowIndex,mid)+binarySum(array,mid+1, highIndex);
        }

    }
}
