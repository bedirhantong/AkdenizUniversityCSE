package com.bedirhan.undergraduate.CSE201_DataStructures.Practice.Recursion.ReversArray;

public class ReverseArray {
    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5};
        reverseArray(arr,0, arr.length-1);
        for (int j : arr) {
            System.out.println(j);
        }
    }
    public static void reverseArray(int[] arr,int lowIndex, int highIndex){
        if (lowIndex < highIndex){
            int temp = arr[highIndex];
            arr[highIndex] = arr[lowIndex];
            arr[lowIndex] = temp;
            reverseArray(arr, lowIndex +1, highIndex -1);
        }
    }
}
