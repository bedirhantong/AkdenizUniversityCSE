package com.bedirhan.undergraduate.CSE201_DataStructures.Practice.Recursion.ArrayMax;

public class ArrayMax {
    public static void main(String[] args) {
        int[] array = {1,2,3,4,5,6,3,5,245,243,24};
        System.out.println(findArrayMax(array,array.length));
    }

    public static int findArrayMax(int[] array,int numberOfElements){
        if (numberOfElements == 1){
            return array[0];
        }
        return Math.max(array[numberOfElements -1],findArrayMax(array, numberOfElements -1));
    }
}
