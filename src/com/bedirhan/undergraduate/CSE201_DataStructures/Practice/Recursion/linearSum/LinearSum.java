package com.bedirhan.undergraduate.CSE201_DataStructures.Practice.Recursion.linearSum;

public class LinearSum {
    public static void main(String[] args) {
        int[] arr = {5,1,2,3,4};
        System.out.println(linearSum(arr, arr.length-1));
    }

    public static int linearSum(int[] array, int lastIndex){
        if (lastIndex == 0)
            return array[0];
        else
            return linearSum(array, lastIndex -1)+ array[lastIndex];
    }
}
