package com.bedirhan.undergraduate.CSE201_DataStructures.Lab.Lab02;

//public class App{
//    public static void main(String[] args) {
//        LinkedList<Integer> myList = new LinkedList<>();
//        myList.addLast(7);
//        myList.addLast(10);
//        myList.addLast(25);
//        myList.print();
//
//
//
//
//    }
//
//}
//
//class LinkedList<E> {
//    Node<E> head;
//
//    private int size;
//    Node<E> tail;
//
//
//    public LinkedList() {
//
//    }
//
//
//    public boolean isEmpty(){
//        return head == null;
//    }
//
//    public int getSize() {
//        return size;
//    }
//
//    private void setSize() {
//        this.size += 1;
//    }
//
//    public void add(Node<E> node,int value){
//        if (isEmpty()){
//            head = node;
//        }
//        else {
//
//        }
//        tail=node;
//        setSize();
//    }
//
//    public void addLast(E value){
//        Node<E> newNode = new Node<>(value);
//        if (isEmpty()){
//            head = newNode;
//        }else {
//            tail.setNext(newNode);
//        }
//        // TODO: burada null olma olayını bir daha sor
//        tail = newNode;
//        setSize();
//    }
//    public void print (Node<E> node){
//        if (node == null){
//            return;
//        } else {
//            System.out.println(node.get_value());
//            print(node.getNext());
//        }
//    }
//
//
//
//    public void print(){
//        print(head);
//
//    }
////    public String print(){
////        return "";
////    }
//
//    //TODO: NEDEN dönüş tipine göre overload edemeyiz : bilgisayar bunu anlayamaz. Dönüş tipinin ne olduğu
//    //TODO: bizim hangisini kullanacağımız seçmemizi de engeller. Bunu bilgisayarın anlamasını bekleyemeyiz.
//}
//class Node<E>{
//    private E _value;
//    private Node<E> next;
//
//    public Node(E value){
//        _value = value;
//    }
//
//    public Node<E> getNext() {
//        return next;
//    }
//
//    public void setNext(Node<E> next) {
//        this.next = next;
//    }
//
//    public E get_value() {
//        return _value;
//    }
//
//    public void set_value(E _value) {
//        this._value = _value;
//    }
//}
public class LinkedListLab2 {
    public static void main(String[] args) throws Exception {
        LinkedList<Integer> myList = new LinkedList<>();
        myList.addLast(5);
        myList.addLast(15);
        myList.addLast(27);
        myList.addLast(91);
        System.out.println(myList.getNode(1).getValue());

    }
}

class LinkedList<E>{
    Node<E> head;
    Node<E> tail;
    public void insertAfter(int index, E value) throws Exception {
        if (getNode(index) == null){
            throw new Exception("No such Element");
        }
        else {
            Node<E> newNode = new Node<>(value);
            Node<E> reqNode = getNode(index);
            Node<E> reqNode2 = getNode(index+1);
            reqNode.setNext(newNode);
            reqNode2.setPrevious(newNode);
            newNode.setNext(reqNode2);
            newNode.setPrevious(reqNode);
        }

    }
    public Node<E> getNode(int index){
        if (index<0 || isEmpty()){
            return null;
        }
        else {
            Node<E> node = head;
            for (int i = 0; i < index; i++) {
                node=node.getNext();
            }
            return node;
        }
    }

    public void insertBefore(int index, E value) throws Exception {
        if (getNode(index) == null){
            throw new Exception("No such Element");
        }
        else {
            Node<E> newNode = new Node<>(value);
            Node<E> reqNode = getNode(index);
            Node<E> reqNode2 = getNode(index-1);
            reqNode.setPrevious(newNode);
            reqNode2.setNext(newNode);
            newNode.setPrevious(reqNode2);
            newNode.setNext(reqNode);
        }
    }

    public boolean isEmpty(){
        return head == null;
    }
    public void addFirst(E value){
        Node<E> newNode =  new Node<>(value);
        if (isEmpty()){
            tail = newNode;
        }
        else {
            head.setPrevious(newNode);
            newNode.setNext(head);
        }
        head = newNode;
    }
    public void addLast(E value){
        Node<E> newNode = new Node<E>(value);
        if(isEmpty()){
            head = newNode;
        }
        else{
            tail.setNext(newNode);
        }
        tail = newNode;

    }

    public void print(){
        this.print(head);
    }
    private void print(Node<E> node){
        if(node == null)
            return;
        System.out.println(node.getValue());
        print(node.getNext());
    }
    public void printReverse(){
        printReverse(tail);
    }
    private void printReverse(Node<E> node){
        if (node == null){
            return;
        }
        System.out.println(node.getValue());
        printReverse(node.getPrevious());
    }
}

class Node<E>{
    private E value;
    private Node<E> next;
    private  Node<E> previous;
    public Node<E> getPrevious() {
        return previous;
    }

    public void setPrevious(Node<E> previous) {
        this.previous = previous;
    }

    public Node(E value){
        this.value = value;
    }

    public E getValue() {
        return value;
    }

    public void setValue(E value) {
        this.value = value;
    }

    public Node<E> getNext() {
        return next;
    }

    public void setNext(Node<E> next) {
        this.next = next;
    }

}