package com.bedirhan.undergraduate.CSE201_DataStructures.Lab.Lab01;

import java.util.Scanner;

public class TicTacToeLab1_20200808033 {
    public static void main(String[] args) {
        TicTacToeLab1_20200808033 ticTacToeLab120200808033 = new TicTacToeLab1_20200808033();
        System.out.println("\t\t\tMINI TIC TAC TOE\n");
        System.out.println("\tRules are as follows");
        System.out.println("- Player 1 can only place an X");
        System.out.println("- Player 2 can only place an O");
        System.out.println("""
                - The first player to put 3 of their own letters in a straight line wins the game. So what does this mean?
                \t- Right to left or vice versa
                \t- Top down or vice versa
                \t- If there are 3 Xs diagonally from top left to bottom right or vice versa, player 1 wins the game.""");
        System.out.println("\n\n");
        System.out.println("The game table is as follows.");
        ticTacToeLab120200808033.printBoard();
        Scanner scanner = new Scanner(System.in);

        while (ticTacToeLab120200808033.isAvailablePositionExists()) {
            if (ticTacToeLab120200808033.getCurrentPlayer() == 1){
                System.out.println("Player 1, please put an X in an empty space on the table.");
            } else if (ticTacToeLab120200808033.getCurrentPlayer() == -1) {
                System.out.println("Player 2, please put an o(it's the letter, not the number) in an empty space on the table.");
            }
            System.out.print("Enter X coordinate : ");
            String i = scanner.next();
            System.out.print("Enter Y coordinate : ");
            String j = scanner.next();
            try{
                ticTacToeLab120200808033.putMark(Integer.parseInt(i),Integer.parseInt(j));
                if (ticTacToeLab120200808033.getWinner() ==  1){
                    System.out.println("\n\tPLAYER 1(X) WON");
                    break;
                } else if (ticTacToeLab120200808033.getWinner() == -1) {
                    System.out.println("\n\tPLAYER 2(O) WON");
                    break;
                } else if (ticTacToeLab120200808033.getWinner() == EMPTY && !(ticTacToeLab120200808033.isAvailablePositionExists())) {
                    System.out.println("\n\tDRAW!!");
                    break;
                }
            }
            catch (IllegalArgumentException e){
                System.out.println(e+"\n"+" Please try again.");
            }
            ticTacToeLab120200808033.printBoard();
            System.out.println("\n");
        }
    }
    public static final int X = 1, O = -1, EMPTY = 0;
    private int[][] board = new int[3][3];
    private int currentPlayer;

    private final static int[][][] winnerIndices = {
            {{0,0}, {0,1}, {0,2}},
            {{1,0}, {1,1}, {1,2}},
            {{2,0}, {2,1}, {2,2}},

            {{0,0}, {1,0}, {2,0}},
            {{0,1}, {1,1}, {2,1}},
            {{0,2}, {1,2}, {2,2}},

            {{0,0}, {1,1}, {2,2}},
            {{0,2}, {1,1}, {2,0}},
    };

    public TicTacToeLab1_20200808033() {
        this.board = new int[3][3];
        this.currentPlayer = X;
    }

    public int getCurrentPlayer() {
        return currentPlayer;
    }

    public void putMark(int i, int j) throws IllegalArgumentException {
        if ((i < 0) || (i > 2) || (j < 0) || (j > 2))
            throw new IllegalArgumentException("Invalid board position.");
        if (board[i][j] != EMPTY)
            throw new IllegalArgumentException("Board position occupied.");
        board[i][j] = currentPlayer;
        currentPlayer = -1 * currentPlayer;
    }

    public boolean isWinner(int player) {

        for(int[][] positions: winnerIndices){
            int sum = 0;
            for(int[] position: positions){
                sum += board[position[0]][position[1]];
            }
            if(sum == player*3)
                return true;
        }
        return false;
    }


    public int getWinner(){
        if (isWinner(X))
            return X;
        else if (isWinner(O))
            return O;
        else
            return EMPTY;
    }

    public void printBoard() {
        for(int[] line: this.board){
            for(int cell: line){
                if(cell == X){
                    System.out.print("X");
                }
                else if(cell == O){
                    System.out.print("O");
                }
                else if(cell == EMPTY){
                    System.out.print("-");
                }
            }
            System.out.println();
        }
    }


    public boolean isAvailablePositionExists(){
        for(int[] line: this.board){
            for(int cell: line){
                if(cell == 0)
                    return true;
            }
        }
        return false;
    }
}

