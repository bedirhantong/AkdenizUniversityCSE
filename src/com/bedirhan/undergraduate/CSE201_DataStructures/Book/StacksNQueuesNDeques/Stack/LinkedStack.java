package com.bedirhan.undergraduate.CSE201_DataStructures.Book.StacksNQueuesNDeques.Stack;


public class LinkedStack<E> implements Stack<E>{
    private Node<E> head;
    private Node<E> tail;

    @Override
    public int size() {
        if (isEmpty()){
            return 0;
        }
        else if (head == tail){
            return 1;
        }
        int num = 0;
        Node<E> temp =  head;

        while ( !(head.getNext() == null) )
        {
            temp = temp.getNext();
            num++;
        }
        return num;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public void push(E e) throws NullPointerException{
        // add into the list
        if (isEmpty()){
            head = new Node<>(e);
            head.setNext(tail);
        }
        else if (head == tail) {
            head = new Node<>(e);
            head.setNext(tail);
        }
        else{
            Node<E> node  = new Node<>(e);
            Node<E> temp = tail;
            node.setPrevious(temp);
            tail = node;
        }
    }

    @Override
    public E top() {
        // gets top element without removing
        if (!isEmpty()){
            if (head == tail){
                return head.getValue();
            }
            return tail.getValue();
        }
        return null;
    }

    @Override
    public E pop() {
        // gets top element with removing it from list
        if (!isEmpty()){
            if (head == tail){
                Node<E> temp = head;
                head.setValue(null);
                return temp.getValue();
            }
            Node<E> temp = tail;
            tail.setValue(null);
            return temp.getValue();
        }
        return null;
    }
}
class Node<E>{
    private E value;
    private Node<E> next;
    private Node<E> previous;
    public Node<E> getPrevious() {
        return previous;
    }

    public void setPrevious(Node<E> previous) {
        this.previous = previous;
    }

    public Node(E value){
        this.value = value;
    }

    public E getValue() {
        return value;
    }

    public void setValue(E value) {
        this.value = value;
    }

    public Node<E> getNext() {
        return next;
    }

    public void setNext(Node<E> next) {
        this.next = next;
    }

}
