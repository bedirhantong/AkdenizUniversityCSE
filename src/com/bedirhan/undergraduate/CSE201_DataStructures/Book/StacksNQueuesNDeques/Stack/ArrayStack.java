package com.bedirhan.undergraduate.CSE201_DataStructures.Book.StacksNQueuesNDeques.Stack;

public class ArrayStack <E> implements Stack<E> {
    public static final int CAPACİTY = 1000;
    private E[] data;
    private int vektor = -1;

    public ArrayStack(){
        this(CAPACİTY);
    }
    public ArrayStack(int capacity){
        data =(E[]) new Object[capacity];
    }

    @Override
    public int size() {
        return vektor +1;
    }

    @Override
    public boolean isEmpty() {
        return vektor ==-1;
    }

    @Override
    public void push(E e) {
        // Inserts an element at the top of the stack.
        if(size() == data.length){
            throw new IllegalStateException("Stack is full");
        }
        data[++vektor] = e;
    }

    @Override
    public E top() {
        /*
    Returns, but does not remove, the element at the top of the stack.
    return top element in the stack (or null if empty)
     */
        if (!isEmpty())
            return data[vektor];
        return null;
    }

    @Override
    public E pop() {
        /*
    Removes and returns the top element from the stack.
    return element removed (or null if empty)
     */
        if (!isEmpty()){
            E e = data[vektor];
            data[vektor] = null;
            vektor--;
            return e;
        }
        return null;
    }
}
