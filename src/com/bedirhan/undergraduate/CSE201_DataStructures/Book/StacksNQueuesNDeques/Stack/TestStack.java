package com.bedirhan.undergraduate.CSE201_DataStructures.Book.StacksNQueuesNDeques.Stack;

import com.bedirhan.undergraduate.CSE201_DataStructures.Book.StacksNQueuesNDeques.Stack.Stack;

public class TestStack {
    public static void main(String[] args) {

    }
    public static Integer[] reverse(Integer[] arr){
        Stack<Integer> s = new ArrayStack<>();
        Integer[] temp = new Integer[arr.length];
        for (Integer integer : arr) {
            s.push(integer);
        }
        // 1,2,3,4

        for (int i = 0; i < arr.length; i++) {
            temp[i] = s.pop();
        }
        arr= temp.clone();
        return arr;
    }

    public static <E> void reverse(E[ ] a) {
        Stack<E> buffer = new ArrayStack<>(a.length);
        for (E e : a) buffer.push(e);
        for (int i=0; i < a.length; i++)
            a[i] = buffer.pop();
        /*
        A generic method that reverses the elements in an array with
        objects of type E, using a stack declared with the interface Stack<E> as its type.
         */
    }
}
