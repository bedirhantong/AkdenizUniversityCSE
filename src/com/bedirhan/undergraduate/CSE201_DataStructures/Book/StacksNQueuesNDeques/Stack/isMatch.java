package com.bedirhan.undergraduate.CSE201_DataStructures.Book.StacksNQueuesNDeques.Stack;

import com.bedirhan.undergraduate.CSE201_DataStructures.Book.StacksNQueuesNDeques.Stack.Stack;

public class isMatch {
    public static void main(String[] args) {
//        if (isMatched("{[]{}()}"))
//            System.out.println("ye");
//        else
//            System.out.println("no");
    }
    public static boolean isMatched(String expression){
        final String opening = "({[";
        final String closing = ")}]";
        Stack<Character> buffer = new ArrayStack<>();
        for (char c : expression.toCharArray()) {
            if (opening.indexOf(c) != -1)
                buffer.push(c);
            else if (closing.indexOf(c) != -1) {
                if (buffer.isEmpty())
                    return false;
                if (closing.indexOf(c) != opening.indexOf(buffer.pop()))
                    return false;
            }
        }
        return buffer.isEmpty();
    }
}
