package com.bedirhan.undergraduate.CSE201_DataStructures.Book.StacksNQueuesNDeques.Stack;

public interface Stack<E> {
    int size(); // Returns the number of elements in the stack

    boolean isEmpty(); // return true if the stack is empty, false otherwise

    void push(E e); // Inserts an element at the top of the stack.

    E top();
    /*
    Returns, but does not remove, the element at the top of the stack.
    return top element in the stack (or null if empty)
     */

    E pop();
    /*
    Removes and returns the top element from the stack.
    return element removed (or null if empty)
     */

}
