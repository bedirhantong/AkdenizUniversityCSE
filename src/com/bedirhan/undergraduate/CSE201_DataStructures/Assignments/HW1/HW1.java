package com.bedirhan.undergraduate.CSE201_DataStructures.Assignments.HW1;

import java.util.HashSet;
import java.util.Set;

public class HW1 {
    static int countCh(String str){
        /*
    Write a short method in any language that counts the number of a,e,i,u characters in a given character string.
     */
        int numOfCh = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.contains("a") || str.contains("e") || str.contains("i") || str.contains("u") ){
                numOfCh++;
            }
        }
        return numOfCh;
    }
    static boolean isDiffEach(float[] arr){
        /*
        Write a method that takes an array of float values and
        determines if all the numbers are different from each other (that is, they are distinct) up to 4 digits after point.
         */
        Set<String> set = new HashSet<>();
        for (float v : arr) {
            String digitsBeforePoint = String.valueOf(v).substring(0, String.valueOf(v).indexOf("."));
            String digitsAfterPoint = String.valueOf(v).substring(String.valueOf(v).indexOf(".") + 1, String.valueOf(v).indexOf(".") + 5);
            String digit = digitsBeforePoint.concat(digitsAfterPoint);
            set.add(digit);
        }
        return set.size() == arr.length;
    }
    static int[] shuffle(int[] arr) {
        /*
        Write a method that takes an array containing the set of all integers in the range 1 to 52 and shuffles it into random order.
        Your method should output each possible order with equal probability.
         */
        int lastIndex = arr.length;
        for (int i = 0; i < arr.length; i++) {
            if (lastIndex == 0) {
                break;
            }
            int random = (int) (Math.random() * lastIndex);
            int temp = arr[random];
            arr[random] = arr[lastIndex - 1];
            arr[lastIndex - 1] = temp;
            lastIndex--;
        }
        return arr;
    }
}
